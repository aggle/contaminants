Required non-standard python modules:

astropy
astroquery (unsure about version requirement)
requests (required by astroquery)


Installation instructions for Linux

astropy:
pip install astropy

astroquery (to get the latest version):
git clone git@github.com:astropy:astroquery.git
cd astroquery
python setup.py install

requests:
pip install requests


Usage

You can always run with --help for a list of arguments

Required arguments:
--stars comma,separated,list,of,stars
--email youremail@you.edu

Optional arguments: 
--filter BAND # H or J, default H
--deltamag (low,high) # parens required. These magnitudes will be added to the
apparent magnitude of the star in the H or J filter. Default: (7,12)
--FOV INSTR # pick the telescope, this sets the FOV. Enter 'custom' to 
set the FOV manually. Current options are HST, P1640, and GPI (default HST).
--outfile FileNameForResults.ext # provide the full file name, including the
extension. Default: "none", indicating no file written


Potential issues/Troubleshooting
--Not sure how it will behave if the files on the Besancon server are in the process of being written when you try to retrieve them
--If you get "0/N stars found in SIMBAD *and* submitted to Besancon", I think
the Besancon server is mad at you and is refusing to let you run jobs. Try
waiting before resubmitting. 
