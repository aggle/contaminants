#!/usr/bin/env python
"""
contaminants.py - query the besancon model of the galaxy to find out how many
                  background stars might be in your FOV
Warning: This program attempts to load the entire besancon results file into memory.
         If it hangs, check your inputs to see if you're requesting a giant file.
Note: requires astroquery version 0.2.dev1630

Jonathan Aguilar
April 1, 2014
"""

import sys, os, io
import argparse
import re
import warnings

import pandas as pd
import numpy as np
import urllib.request, urllib.error, urllib.parse
from ftplib import FTP

from astropy import coordinates as coord
from astropy import units
from astropy.table import Table, Column
from astroquery.simbad import Simbad
from astroquery.besancon import Besancon

def getStarNamesFromFile(starfile):
    """
    Return a pandas Series of the star names.
    Be able to handle the case where a file has just one star
    """
    s = np.genfromtxt(starfile,dtype=str,delimiter='\n')
    stars = pd.Series([s.item()]) if (s.size==1) else pd.Series(s)
    return stars


def getBesanconFileName(besancon_response):
    """
    Searches the HTML response from the Besancon server and extracts the filename.
    """
    pattern = "parameter file [0-9]*.[0-9]*.par"
    filename = re.search(pattern,besancon_response.text)
    if not filename:
        print("File not found")
        return ''#False
    filename = filename.group().split()[-1]
    filename = os.path.splitext(filename)[0]+'.resu'
    return filename


def downloadBesanconFile(path,filename,email,starname=''):
    """
    Download a Besancon model file to the hard drive
    Returns False if download failed; True if succeeded
    """
    try:
        ftp = FTP('sasftp.obs-besancon.fr','anonymous',email)
        ftp.cwd('/modele/modele2003')
        ftp.retrbinary('RETR {0}'.format(filename), 
                       open(os.path.join(path,filename),'wb').write)
    except:
        ftp.close()
        return False
    # wait for file to finish transfering
    '''
    try:
        with io.FileIO(os.path.join(path,filename), "r+") as fileObj:
            if(os.path.isfile(os.path.join(path,filename))):
                if starname != '':
                    print star + " -- " + filename + " has completely downloaded."
                else:
                    print "File " + filename + " has finished downloading."
    except IOError as ioe:
        print str(ioe)
        '''
    return True
    ftp.close()

def getNumContaminants(filename):
    with open(filename,'r') as f:
        f.seek(-100,2)
        try:
            numstars=int(f.readlines()[-1].strip().split(':')[-1])
        except:
            numstars=np.nan
    return numstars
        
def getRemoteNumContaminants(filename):
    """
    If the file is not found, returns NaN.
    What if the file is currently being written?
    Currently not used
    """
    try:
        url = urllib.request.urlopen('ftp://sasftp.obs-besancon.fr/modele/modele2003/'+filename)
    except urllib.error.URLError:
        return np.nan
    try:
        numstars=int(url.readlines()[-1].strip().split(':')[-1])
    except:
        return np.nan
    return numstars

def checkStarData(simbad_res, starname, filter_name):
    """
    Check that SIMBAD returned the critical information for a star
    Returns True if the star passes inspection; false otherwise
    """
    if not bool(simbad_res['MAIN_ID']):
        print(starname, 'not found in SIMBAD, skipping...')
        return False
    # check that identifier is there
#    alternate_ids = simbad['ID_HIP_HD_CD_GSC_LSD_SSS_TWA_TYC'][0]
#    if type(alternate_ids) == str:
#        alternate_ids = '{0}{1}'.format(*tuple(alternate_ids.split()))
#        if alternate_ids != starname:
#            return False
#    else:
#        if type(alternate_ids) == list:
#            alternate_ids = ['{0}{1}'.format(*tuple(a)) for a in alternate_ids]
    return True


def querySimbad(starname):
    """
    Simbad query, with wrapper to make error handling easier/cleaner
    One star at a time, for safer indexing
    """
    try:
        with warnings.catch_warnings():
            warnings.simplefilter("ignore")
            # SIMBAD throws a warning if a star isn't found; we can handle that so
            # we ignore this warning
            #if starnames.size == 1:
            #    simbad_res = Simbad.query_object(starnames)
            #else:
            simbad_res = Simbad.query_object(starname)
    except:
        print("Query failed")
        sys.exit(0)
    return simbad_res




#####################################################
# SIMBAD parameters
#####################################################
simbad_id_field='id(BRI|CD|G|GJ|GL|GSC|HD|HIP|LDS|LHS|LP|PS|SSS|TVLM|TWA|TYC)'
try:
    Simbad.add_votable_fields('flux(V)','flux(H)','flux(J)')
    Simbad.add_votable_fields('coo(Gal)')
    Simbad.add_votable_fields('sptype')
    Simbad.add_votable_fields(simbad_id_field)
except KeyError:
    pass
# build this last field name, because it's complicatedsimbad_id_field='id(BRI|CD|HD|HIP|GSC|LSD|SSS|TWA|TYC)'
simbad_id_field=simbad_id_field.replace('(','_')
simbad_id_field=simbad_id_field.replace('|','_')
simbad_id_field=simbad_id_field.replace(')','')
simbad_id_field=simbad_id_field.upper()

#####################################################
# Besancon query parameters -- defaults
# not all possible entries are listed
#####################################################
# absolute magnitude limits
absmag=(-7,20)
# apparent magnitude limits
mag_max=9
mag_min=24
maglimits = {'U':(-99,99),
             'B':(-99,99),
             'V':(-99,99),
             'R':(-99,99),
             'I':(-99,99),
             'J':(-99,99),
             'H':(-99,99),
             'K':(-99,99),
             'L':(-99,99)}
# colors
colors = {"B-V":(-99,99),
          "U-B":(-99,99),
          "V-I":(-99,99),
          "V-K":(-99,99)}
# fields of view solid angle, in degrees
instr_field = {'HST':(3./3600)**2,
               'P1640':(3.8/3600)**2,
               'GPI':(2.8/3600)**2
               }
#instr_field = {'HST':(3*units.arcsec).to(units.deg),
#               'P1640':(3.8*units.arcsec).to(units.deg),
#               }

parser = argparse.ArgumentParser(description="Get contamination numbers for a provided list of stars")
parser.add_argument('--stars',
                    required=True,
                    dest='stars',
                    action='store',
                    help='CSV list of stars, or path to a file containing SIMBAD-searchable star identifiers, one per line')


parser.add_argument('--email', 
                    required=True,
                    dest='email',
                    action='store',
                    help='email address to send results')
parser.add_argument('--filter',
                    required=False,
                    default='J',
                    dest='filter',
                    action='store',
                    help='choose H or J band filter (J)')
parser.add_argument('--deltamag',
                    required=False,
                    default='(8,14)',
                    dest='deltamag',
                    action='store',
                    help='upper and lower delta mag in format (ul,ll) WITH PARENS, for the chosen filter ( (7,12) )')
parser.add_argument('--FOV',
                    required=False,
                    default='HST',
                    dest='FOV',
                    action='store',
                    help='One of HST, P1640 GPI or "custom", to set the field size. "Custom" will prompt you. (HST)')
parser.add_argument('--outfile',
                    required=False,
                    default='none', 
                    dest='outfile',
                    action='store',
                    help='Name for the file where the results will be stored (./results/results.csv); use none for to not write a file')
parser.add_argument('--fieldsize',
                    required=False,
                    default=1,
                    dest='fieldsize',
                    type=float,
                    action='store',
                    help='(Depracated) Size of the field to search in [deg^2] (1)')
parser.add_argument('--datapath',
                    required=False,
                    default='./data/',
                    dest='datapath',
                    action='store',
                    help='Path for downloading possible very large files off the Besancon server')
parser.add_argument('--verifyStarNames',
                    required=False,
                    default=False,
                    dest='verifyStarNames',
                    action='store_true',
                    help='Print alternate star designations to verify you have the right stars')

if __name__ == "__main__":
    args = parser.parse_args()
    print("Arguments:")
    for arg in list(args.__dict__.items()):
        print("{0}:\t\t{1}".format(*arg))

    if args.filter:
        if (args.filter.upper() == 'J') or (args.filter.upper() == 'H'):
            filter_name = 'FLUX_{0}'.format(args.filter.upper())
        else:
            print('Bad filter "{0}": please select "J" or "H".')
            sys.exit()
    if args.deltamag:
        deltamag = tuple(map(int,args.deltamag[1:-1].split(',')))

    if args.FOV=='custom':
        instr_field['custom']=(float(input("Enter desired FOV [mas]: "))/3600)**2
        

    # check if outfile already exists:
    while os.path.isfile(args.outfile):
        newname = input("{0} already exists! Enter a new name, or leave blank to overwrite. ".format(args.outfile)) 
        if newname: 
            args.outfile = newname
        else: # newname is blank
            break

    starnames = []
    if len(args.stars.split('.')) > 1:
        starnames = getStarNamesFromFile(args.stars)
    else:
        starnames = args.stars.split(',')

    # data structures - be able to handle only one star
    besancon_data = pd.DataFrame({'stars' : starnames,
                                  'found' : [None for s in starnames],
                                  'Spec_type': [None for s in starnames],
                                  'gal-lon' : [np.nan for s in starnames],
                                  'gal-lat' : [np.nan for s in starnames],
                                  'Vmag' : [np.nan for s in starnames],
                                  'Jmag' : [np.nan for s in starnames],
                                  'Hmag' : [np.nan for s in starnames],
                                  '{0}mag_range'.format(args.filter) : [None for s in starnames],
                                  'files' : [None for s in starnames],
                                  'contaminants' : [np.nan for s in starnames],
                                  'FOVcontaminants' : [np.nan for s in starnames]},
                                 columns=['stars','found','Spec_type',
                                          'gal-lon','gal-lat',
                                          'Vmag','Jmag','Hmag',
                                          '{0}mag_range'.format(args.filter),
                                          'files',
                                          'contaminants', 
                                          'FOVcontaminants'],
                                 dtype=object)

    # Query SIMBAD and set star parameters
    print('Querying SIMBAD...')
    for i,row in besancon_data.iterrows():
        simbad_res = querySimbad(row['stars'])
        if simbad_res == None:
            print('{0} SIMBAD query failed'.format(row['stars']))
            besancon_data.loc[i,'found'] = False
            continue
        if not checkStarData(simbad_res, row['stars'], filter_name):
            besancon_data.loc[i,'files'] = None
            continue

        # visually validate that the star is the correct one by checking the alternate names
        #print '{0} ?= {1} {2}'.format(row['stars'], 
        #                              simbad_res[simbad_id_field][0], 
        #                              simbad_res['MAIN_ID'][0])

        galcoord = coord.SkyCoord(l=simbad_res['RA_Gal'][0], 
                                  b=simbad_res['DEC_Gal'][0],
                                  frame=coord.Galactic,
                                  unit=(units.degree,units.degree))
        gal_l, gal_b = galcoord.l.degree, galcoord.b.degree
        mag = simbad_res[filter_name][0]
        if not mag: # bool(masked) evaluates to False
            magrange = tuple((mag_max,mag_min))
        else:
            magrange = tuple((mag+deltamag[0],mag+deltamag[1]))
        besancon_data.loc[i,'found'] = True
        besancon_data.loc[i,'Spec_type'] = simbad_res['SP_TYPE'][0]
        besancon_data.loc[i,'gal-lon'] = gal_l
        besancon_data.loc[i,'gal-lat'] = gal_b
        besancon_data.loc[i,'Vmag'] = simbad_res['FLUX_V'][0]
        besancon_data.loc[i,'Hmag'] = simbad_res['FLUX_H'][0]
        besancon_data.loc[i,'Jmag'] = simbad_res['FLUX_J'][0]
        besancon_data.loc[i,'{0}mag_range'.format(args.filter)] = magrange
        
    print('Querying Besancon...')
    # Query Besancon
    for i,row in besancon_data.iterrows():
        # skip if star not in SIMBAD
        if not row['found']: continue
        # get query parameters
        # submit jobs
        maglimits_tmp = {args.filter: row['{0}mag_range'.format(args.filter)]} #maglimits.copy()
        maglimits_tmp[args.filter]=row['{0}mag_range'.format(args.filter)]
        try:
            besancon_response = Besancon.query_async(email = args.email,
                                                     glon = row['gal-lon'],
                                                     glat = row['gal-lat'],
                                                     smallfield = True,
                                                     area = 1, # 1 deg^2 old:args.fieldsize,
                                                     absmag_limits = absmag, # global
                                                     colors_limits = colors, # global
                                                     mag_limits = maglimits_tmp, 
                                                     verbose = True)
        except:
            print("Besancon query error, skipping {0}".format(row['stars']))
            continue
        bsfilename = re.search(Besancon.result_re,besancon_response.text).group()
        besancon_data.loc[i,'files'] = bsfilename
        if not i%20: print('...',i,'/',len(besancon_data), 'queries submitted')

    # wait for emails to come in
    print("{0}/{1} stars submitted to Besancon".format(\
        besancon_data['files'].dropna().size, besancon_data['files'].size))
    input("Press 'Enter' when you get the emails that the models are finished running\n")
    print("Retreiving files from Besancon server.")
    # get the contaminants in the field
    while len(besancon_data[(besancon_data['contaminants'].isnull()) & (besancon_data['files'].notnull())]):
        for i,row in besancon_data[(besancon_data['contaminants'].isnull()) & (besancon_data['files'].notnull())].iterrows():
            if downloadBesanconFile(args.datapath,row['files'], args.email):
                besancon_data.loc[i,'contaminants'] = getNumContaminants(os.path.join(args.datapath,row['files']))
                os.remove(os.path.join(args.datapath,row['files']))
        # give option to exit manually
        if len(besancon_data[(besancon_data['contaminants'].isnull()) & (besancon_data['files'].notnull())]):
            try:
                print("The following entries haven't yet been processed:")
                print(besancon_data[['stars','files']][besancon_data['contaminants'].isnull()])
                input("Press 'Enter' to try to find them, or Ctrl-C to stop looking\n")
            except KeyboardInterrupt:
                break
    
    # normalize contaminants by the instrument field of view 
    besancon_data['FOVcontaminants'] = besancon_data['contaminants']*instr_field[args.FOV] #stars/deg^2*FOV
    print("Density of background objects [stars/FOV]: ")
    for i, row in besancon_data[besancon_data['FOVcontaminants'].notnull()].iterrows():
        print("{0}:\t{1}".format(besancon_data.ix[i]['stars'],besancon_data.ix[i]['FOVcontaminants']))

    if (len(besancon_data[besancon_data['contaminants'].isnull()]) != 0):
        print('The following entries could not be processed:')
        print(besancon_data[besancon_data['contaminants'].isnull()]['stars'])
    print("\n")
    if args.outfile.lower() == 'none':
        pass
    else:
        outfilename = os.path.join('.', args.outfile)
        print("Results stored in {0}".format(outfilename))
        besancon_data[['stars','Spec_type',
                       'gal-lon','gal-lat',
                       'Vmag','Jmag','Hmag',
                       '{0}mag_range'.format(args.filter),
                       'contaminants',
                       'FOVcontaminants']].to_csv(outfilename, 
                                                  mode='w', sep='\t',
                                                  index=False, na_rep='--')
g
